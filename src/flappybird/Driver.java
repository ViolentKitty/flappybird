package flappybird;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Driver extends Application{

    private Timeline timeline;
    private ImageView bird;
    private List<Rectangle> obstacles;
    private int score;

    private int previousGapLocation;

    @Override
    public void start(Stage stage) {

        // create javafx contents
        Pane pane = new Pane();
        Scene scene = new Scene(pane, stage.getMaxWidth(), stage.getMaxHeight(), Color.LIGHTCYAN);
        pane.setPrefSize(scene.getWidth(), scene.getHeight());

        stage.setMaximized(true);
        stage.setTitle("Flappy Bird");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();

        // create bird
        Image iconImg = new Image("https://cdn1.iconfinder.com/data/icons/angry-icons-by-femfoyou/501/redbird.png");
        bird = new ImageView(iconImg);
        bird.setFitHeight(40);
        bird.setFitWidth(40);
        int birdStartX = (int) pane.getWidth() / 50;
        int birdStartY = (int) pane.getHeight() / 4;
        bird.relocate(birdStartX * 3, birdStartY);
        pane.getChildren().add(bird);

        // create score text
        Text scoreText = new Text(25, 25, "Score: " + score);
        score = 0;
        scoreText.setFill(Color.BLACK);
        scoreText.setFont(Font.font("Georgia", FontWeight.BOLD, 25));
        pane.getChildren().add(scoreText);

        // initialize other variables
        obstacles = new ArrayList<>();

        timeline = new Timeline(new KeyFrame(Duration.millis(20), new EventHandler<>() {
            double time = 0.0;
            double dyBird = 0;
            double gravity = 7.5;
            double dxObstacle = -5;
            double timeUntilNextColumn = 0;

            @Override
            public void handle(ActionEvent t) {
                // update score
                score += 1;
                scoreText.setText("Score: " + score);

                // calculate new bird position
                dyBird += gravity * 0.5 * Math.pow(time, 2);

                // handle space bar click (jump)
                scene.setOnKeyPressed(e -> {
                    if (e.getCode() == KeyCode.SPACE) {
                        dyBird = -4.5;
                        time = .1;
                    }
                });

                // move the columns and check for collision
                for (Rectangle obstacle : obstacles) {
                    boolean collision = checkForCollision(pane, bird, obstacle);
                    if (collision) {
                        gameover(pane);
                    }
                    // move the columns left
                    obstacle.setLayoutX(obstacle.getLayoutX() + dxObstacle);
                }

                // move bird to new position
                double newY = bird.getLayoutY() + dyBird;
                bird.relocate(bird.getLayoutX(), newY);

                // generates a column every 1.5 seconds and destroys old columns
                if (timeUntilNextColumn <= 0) {
                    createObstacles(pane);
                    destroyObstacles(pane);
                    timeUntilNextColumn = 1500;
                }
                timeUntilNextColumn -= 20;

                time += 0.01;
            }
        }));

        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }

    /*
     *  generates two columns with a gap in between and
     *  positions them on the right side of screen
     */
    private void createObstacles(Pane pane) {
        int columnGap = 150;

        // generate a random place for the gap in column
        int min = 100;
        int max = (int)(pane.getHeight()) - columnGap - 50;

        if (obstacles.size() != 0) {
            if (previousGapLocation - 2 * columnGap > min) {
                min = previousGapLocation - 2 * columnGap;
            }
            if (previousGapLocation + 2 * columnGap < max) {
                max = previousGapLocation + 2 * columnGap;
            }
        }

        int rand = (int)(Math.random() * (max - min + 1)) + min;
        previousGapLocation = rand;

        // create top part of column
        Rectangle obstacleOne = new Rectangle(50, rand, Color.MAROON);
        obstacleOne.relocate(pane.getWidth(), 0);

        // create bottom part of column
        int obstacleTwoHeight = (int)(pane.getHeight()) - rand - columnGap;
        int obstacleTwoStart = rand + columnGap;
        Rectangle obstacleTwo = new Rectangle(50, obstacleTwoHeight, Color.MAROON);
        obstacleTwo.relocate(pane.getWidth(), obstacleTwoStart);

        // add the column parts to obstacles
        obstacles.add(obstacleOne);
        obstacles.add(obstacleTwo);
        pane.getChildren().addAll(obstacleOne, obstacleTwo);
    }

    /*
     *  goes through all obstacles and gets rid of the ones
     *  that have passed through the left border of screen
     */
    private void destroyObstacles(Pane pane) {
        Iterator<Rectangle> it = obstacles.iterator();

        while (it.hasNext()) {
            Rectangle obstacle = it.next();
            if (obstacle.getLayoutX() <= -50) {
                pane.getChildren().remove(obstacle);
                it.remove();
            }
        }
    }

    /*
     *  returns true if the bird is in contact with a column or window border
     */
    private boolean checkForCollision(Pane pane, ImageView bird, Rectangle column) {
        return
            bird.getLayoutY() <= 0 ||
            bird.getLayoutY() >= pane.getHeight() ||
            bird.getBoundsInParent().intersects(column.getBoundsInParent());
    }

    /*
     *  stops animation and displays final score
     */
    private void gameover(Pane pane) {
        timeline.stop();

        // lower opacity of all nodes except score
        for (Node node : pane.getChildren()) {
            if (!(node instanceof Text)) {
                node.setOpacity(0.3);
            }
        }

        Text finalScore = new Text();
        finalScore.setText("GAME  OVER!");
        finalScore.setFill(Color.CRIMSON);
        finalScore.setFont(Font.font("Georgia", FontWeight.BOLD, 60));

        // calculate positions so that gameover text is set on the stage center
        new Scene(new Group(finalScore));
        double textHeight = (finalScore.getLayoutBounds().getHeight());
        double textWidth = (finalScore.getLayoutBounds().getWidth()) / 2;
        double textY = (pane.getHeight() / 2) - textHeight;
        double textX = (pane.getWidth() / 2) - textWidth;
        finalScore.relocate(textX, textY);

        pane.getChildren().add(finalScore);
    }

}